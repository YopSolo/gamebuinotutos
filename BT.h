#ifndef BLANKTEMPLATE_BT_H_
#define BLANKTEMPLATE_BT_H_
#include "Config.h";

class BT
{
public:
  static BT& instance()
  {
    static BT *instance = new BT();
    return *instance;
  }

  String getVersion(){
      return "0.0";
  }

private:
  BT() {}
};

#endif