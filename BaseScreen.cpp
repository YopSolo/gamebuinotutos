#include <Gamebuino-Meta.h>
#include "BaseScreen.h"
#include "Config.h"

void BaseScreen::start()
{
    _screenState = SCREEN_MAIN;

    // reset transition
    _transitionFrameCountLimit = 22;
    // reset flash
    _flashFrameCountLimit = 0;
    _flashColor = DARKGRAY;
    // reset lights
    _lightsFrameCountLimit = 0;
    _lightsColor = WHITE;
    // reset shake
    _nbShakes = 0;
    _shakePower = 0;
    _shakeX = 0;
    _shakeY = 0;
    // reset typewriter
    _currentText = "";
    _twIndex = 0;
    _twSpeed = 1;
}

void BaseScreen::onEnterFrame()
{
}

void BaseScreen::end()
{
}

String BaseScreen::getScreenName()
{
    return _screenName;
}

void BaseScreen::updateMainState()
{
    // ====================================
    // == Flash
    if (_flashFrameCountLimit > 0)
    {
        _isFlashing = (_flashFrameCountLimit == 16 || _flashFrameCountLimit == 15 
                        || _flashFrameCountLimit == 12 || _flashFrameCountLimit == 11
                        || _flashFrameCountLimit == 8 || _flashFrameCountLimit == 7
                        || _flashFrameCountLimit == 6 || _flashFrameCountLimit == 5);
        if(_isFlashing){
            gb.lights.fill(_lightsColor);
        }else{
            gb.lights.clear();
        }
        _flashFrameCountLimit--;
    }

    // ====================================
    // == Lights
    if (_lightsFrameCountLimit > 0)
    {
        if (_lightsFrameCountLimit > 1)
        {
            gb.lights.fill(_lightsColor);
        }
        else if (_lightsFrameCountLimit == 1)
        {
            gb.lights.clear();
        }
        _lightsFrameCountLimit--;
    }

    // ====================================
    // == Shakes
    if (_nbShakes > 0)
    {
        _shakeX = random(-_shakePower, _shakePower);
        _shakeY = random(-_shakePower, _shakePower);
        if (_nbShakes == 1)
        {
            _shakeX = 0;
            _shakeY = 0;
        }
        _nbShakes--;
    }
}

void BaseScreen::updateTransitionInState()
{
}

void BaseScreen::updateTransitionOutState()
{
}

void BaseScreen::transitionIN()
{
    _screenState = SCREEN_TRANSITION_IN;
}

void BaseScreen::transitionOUT()
{
    _screenState = SCREEN_TRANSITION_OUT;
}

void BaseScreen::shake(uint8_t pNbQuakes = 10, uint8_t pPower = 5)
{
    _nbShakes = pNbQuakes;
    _shakePower = pPower;
}

void BaseScreen::flash(Color pColorValue)
{
    _flashFrameCountLimit = 16;
    _flashColor = pColorValue;
}

void BaseScreen::lights(Color pColorValue)
{
    _lightsFrameCountLimit = 7;
    _lightsColor = pColorValue;
}

void BaseScreen::typewrite(String pText, uint8_t pSpeed, uint8_t pNbCaracs)
{
    _currentText = pText;
    _twSpeed = min(max(1, pSpeed), 8);                        // contraint la vitesse entre 1 et 8
    uint8_t nbCaracs = min(pNbCaracs, _currentText.length()); // contraint le nombre de caractères pour ne pas dépasser la taille de la chaine

    if ((gb.frameCount % _twSpeed) == 0)
    {
        if (_twIndex + pNbCaracs <= _currentText.length())
        {
            _twIndex += pNbCaracs;
            gb.sound.playTick();
        }
        else
        {
            _twIndex = _currentText.length();
        }
    }
    gb.display.print(_currentText.substring(0, _twIndex));
}