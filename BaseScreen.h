#ifndef BLANKTEMPLATE_BASESCREEN_H_
#define BLANKTEMPLATE_BASESCREEN_H_
#include <Gamebuino-Meta.h>
#include "Config.h"

class BlankTemplate;

class BaseScreen
{
public:
	enum ScreenState
	{
		SCREEN_TRANSITION_IN,
		SCREEN_MAIN,
		SCREEN_TRANSITION_OUT,
	};

	virtual void init() = 0;
	virtual void start() = 0;
	virtual void onEnterFrame() = 0;
	virtual void end() = 0;
	virtual void transitionIN();
	virtual void transitionOUT();

	String getScreenName();

	void updateMainState();
	void updateTransitionInState();
	void updateTransitionOutState();
	
	void shake(uint8_t pNbShakes, uint8_t pPower);
	void flash(Color pColorValue);
	void lights(Color pColorValue);
	void typewrite(String pText, uint8_t pSpeed = 1, uint8_t pNbCaracs = 1);
	// void showMessageBox();
	// void hideMessageBox();
	// void onValidMessageBox();

// /!\	toutes ces valeurs doivent etre reset au start du base screen afin de toujours repartir sur des valeurs par defaut apres un changement d'ecran
protected:
	ScreenState _screenState = SCREEN_MAIN;
	String _screenName = "BaseScreen";
	String _nextScreen = "BaseScreen";

	// Transition
	uint8_t _transitionFrameCountLimit = 22;	
	// Flash
	uint8_t _flashFrameCountLimit = 0;
	Color _flashColor = DARKGRAY;
	bool _isFlashing = false;
	// Lights
	uint8_t _lightsFrameCountLimit = 0;
	Color _lightsColor = WHITE;
	// Shake
	uint8_t _nbShakes = 0;
	uint8_t _shakePower = 0;
	int8_t _shakeX = 0;
	int8_t _shakeY = 0;
	// Typewriter
	String _currentText = "";
	uint8_t _twIndex = 0;
	uint8_t _twSpeed = 1; // 1 minimum (fast)

private:
	//
};

#endif