#include <Gamebuino-Meta.h>
#include "Config.h"
#include "BlankTemplate.h"
#include "ScreenTitle.h"
#include "ScreenGame.h"
#include "ScreenEnd.h"


// [step 1][*] creer tous les ecrans et passer d'un ecran a l'autre en utilisant une ref vers le BlankTemplate (qui possède les methode de load screen, next screen etc.)
//      - passer passer tous les argument dans le constructeur (&BT, _screenName, _nextScreen)
//      - mettre la ref du BT dans la classe BaseScreen pour ne pas avoir a creer une ref par ecran
// [step 2] découpler les BaseScreen du BlankTemplate en creant un systeme de message (pattern observer, event broker)
// [step 3] creer/detruire dynamiquement les écrans a partir de la string : ScreenName 
BlankTemplate::BlankTemplate()
{
    _screens[0] = new ScreenTitle(*this);
    _screens[0]->init(); // [*] a virer et passer les infos _nextScreen et _ScreenName dans le constructeur
    _screens[1] = new ScreenGame(*this);
    _screens[1]->init();
    _screens[2] = new ScreenEnd(*this);
    _screens[2]->init();

    _currentIndex = 0;
    _currentScreen = _screens[_currentIndex];
}

BlankTemplate::~BlankTemplate()
{
    // delete _currentScreen;
}

void BlankTemplate::begin()
{
    _currentScreen->start();
}

void BlankTemplate::update()
{
    _currentScreen->onEnterFrame();
}

void BlankTemplate::loadScreen(String pScreenName)
{
    for (uint8_t i = 0; i < NB_SCREENS; i++)
    {
        if (_screens[i]->getScreenName().equals(pScreenName))
        {
            _currentIndex = i;
            _currentScreen = _screens[_currentIndex];
            _currentScreen->start();
            break;
        }
    }
}

void BlankTemplate::loadScreen(uint8_t pIndex)
{
    if (pIndex < NB_SCREENS)
    {
        _currentIndex = pIndex;
        _currentScreen = _screens[_currentIndex];
        _currentScreen->start();
    }
}

void BlankTemplate::prevScreen()
{
    if (_currentIndex - 1 > -1)
    {
        _currentIndex--;
        _currentScreen = _screens[_currentIndex];
        _currentScreen->start();
    }
}

void BlankTemplate::nextScreen()
{
    if (_currentIndex + 1 < NB_SCREENS)
    {
        _currentIndex++;
        _currentScreen = _screens[_currentIndex];
        _currentScreen->start();
    }
}