#ifndef BLANKTEMPLATE_BLANKTEMPLATE_H_
#define BLANKTEMPLATE_BLANKTEMPLATE_H_

#include "Config.h"
#include "BaseScreen.h"

class BlankTemplate
{
public:
    BlankTemplate();
    ~BlankTemplate();

    void begin();
    void update();
    void nextScreen();
    void prevScreen();
    void loadScreen(String pScreenName);
    void loadScreen(uint8_t pIndex);

protected:

private:
    uint8_t _currentIndex = 0;
    BaseScreen *_screens[NB_SCREENS];
    BaseScreen *_currentScreen = nullptr;

};

#endif