#ifndef BLANKTEMPLATE_CONFIG_H_
#define BLANKTEMPLATE_CONFIG_H_

#include <Gamebuino-Meta.h>

#define GAME_NAME "Game Name"

#define STAGEWIDTH 159
#define STAGEWIDTH2 79
#define STAGEHEIGHT 127
#define STAGEHEIGHT2 63

#define FRAME_RATE 25
#define dt 0.04

#define NB_SCREENS 3

#endif