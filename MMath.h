#ifndef BLANKTEMPLATE_MMATH_H_
#define BLANKTEMPLATE_MMATH_H_

class MMath
{
public:
	/**
	 * Distance Between two 2D point
	 * param  pX0,pY0 1st point
	 * param  pX1,pY1 2nd point
	 * return distance between 2 points
	 */
	static float distance(float pX0, float pY0, float pX1, float pY1);

	/**
	 * Squared distance between p0 and p1, faster than the real distance function beacause no square root (Usefull to compare 2 distances)
	 * param  pX0,pY0 1st point
	 * param  pX1,pY1 2nd point
	 * return    distance between points
	 */
	static float distance2(float pX0, float pY0, float pX1, float pY1);

	/**
	 * Smooth step & smoother step
	 * The function receives a real number pX as an argument and returns 0 if pX is less than or equal to the left edge, 1 if pX is greater than or equal to the right edge, and smoothly interpolates between 0 and 1.
	 * https://en.wikipedia.org/wiki/Smoothstep
	 * https://www.youtube.com/watch?v=60VoL-F-jIQ&ab_channel=TheArtofCode
	 * if you are looking for a linear interpolation use : lerp
	 * param {*} pEdge0, left edge
	 * param {*} pEdge1, right edge with e0 < e1
	 * param {*} pX, input
	 * 
	 * return a value  0 ≤ x ≤ 1
	 */
	static float smoothstep(float pEdge0, float pEdge1, float pX);
	static float smootherstep(float pEdge0, float pEdge1, float pX);
	static float inverseSmoothstep(float pX);		

	/**
	 * EaseIN (based on pow(x,3)) & EaseOut (based on 'inverse' of pow(x,4))
	 * param {*} pEdge0, left edge
	 * param {*} pEdge1, right edge with e0 < e1
	 * param {*} pX, input
	 * 
	 * return a value  0 ≤ x ≤ 1
	 */
	static float easeIN(float pEdge0, float pEdge1, float pX);
	static float easeOUT(float pEdge0, float pEdge1, float pX);

	/**
	 * Clamp, Clamps pX to be between Min and Max, inclusive
	 * param {*} pX, input
	 * param {*} pLowerlimit,
	 * param {*} pUpperlimit,
	 * 
	 * return a value  pLowerlimit ≤ x ≤ pUpperlimit
	 */
	static float clamp(float pX, float pLowerlimit, float pUpperlimit);

	/**
	 * lerp, performs a linear interpolation between two values 
	 * if you are looking for a non linear interpolation use smootstep
	 * https://en.wikipedia.org/wiki/Linear_interpolation
	 * param {*} value 1
	 * param {*} value 2
	 * param {*} pAlpha ranges from 0-1
	 * 
	 * return a value 0 ≤ x ≤ 1
	 */
	static float lerp(float pA, float pB, float pAlpha);

	/**
	 * Quasi random number generator
	 * golden ratio (x2 = x + 1), inverse of golden ratio = 1/1.61803398875 = 0.618033988749894848
	 * https://fr.wikipedia.org/wiki/Nombre_d%27or
	 * 
	 * platic number (x3 = x + 1) , inverse of plastic number = 1/1.3247 = 0.7548776662466927
	 * https://fr.wikipedia.org/wiki/Nombre_plastique
	 * 
	 * param {*} minimum of the interval
	 * param {*} maximum of the interval
	 * param {*} the seed (an input value, ex : gb.frameCount)
	 * 
	 * return a value pMin ≤ x ≤ pMax or .0 if pMin > pMax
	 */
	static double ran(int16_t pMin, int16_t pMax, uint16_t pSeed);

private:
	MMath() {}
};

float MMath::distance(float pX0, float pY0, float pX1, float pY1)
{
	return sqrt(pow(pX1 - pX0, 2) + pow(pY1 - pY0, 2));
}

float MMath::distance2(float pX0, float pY0, float pX1, float pY1)
{
	return pow(pX1 - pX0, 2) + pow(pY1 - pY0, 2);
}

float MMath::smoothstep(float pEdge0, float pEdge1, float pX)
{
	pX = clamp((pX - pEdge0) / (pEdge1 - pEdge0), 0.0, 1.0);
	return pX * pX * (3 - 2 * pX);
}

float MMath::smootherstep(float pEdge0, float pEdge1, float pX)
{
	pX = clamp((pX - pEdge0) / (pEdge1 - pEdge0), 0.0, 1.0);
	return pX * pX * pX * (pX * (pX * 6 - 15) + 10);
}

float MMath::inverseSmoothstep(float pX)
{
	return 0.5 - sin(asin(1.0 - 2.0 * pX) / 3.0);
}

// x^3
float MMath::easeIN(float pEdge0, float pEdge1, float pX)
{
	pX = clamp((pX - pEdge0) / (pEdge1 - pEdge0), 0.0, 1.0);
	return pX * pX * pX;
}

// 1-(x-1)^4
// x(x3 - 4x2 + 6x - 4)
float MMath::easeOUT(float pEdge0, float pEdge1, float pX)
{
	pX = clamp((pX - pEdge0) / (pEdge1 - pEdge0), 0.0, 1.0);
	return -1*((pX*pX*pX*pX) - 4*(pX*pX*pX) + 6*(pX*pX) - 4*(pX));
}

float MMath::clamp(float pX, float pLowerlimit, float pUpperlimit)
{
	if (pX < pLowerlimit)
		pX = pLowerlimit;
	if (pX > pUpperlimit)
		pX = pUpperlimit;
	return pX;
}

float MMath::lerp(float pA, float pB, float pAlpha)
{
	return (1.0f - pAlpha) * pA + pAlpha * pB;
}

double MMath::ran(int16_t pMin, int16_t pMax, uint16_t pSeed)
{
	if(pMin>= pMax){
		return .0;
	}
	// inverse platic : 0.7548776662466927
	// inverse golden : 0.618033988749894848
	double ran = fmod(0.618033988749894848 * pSeed, 1);
	return pMin + (ran * (pMax - pMin));
}

#endif