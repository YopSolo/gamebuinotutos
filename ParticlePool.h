#ifndef BLANKTEMPLATE_PARTICLEPOOL_H_
#define BLANKTEMPLATE_PARTICLEPOOL_H_
#include <Gamebuino-Meta.h>

// [*] améliorer pour ne pas avoir a faire une recherche dans la pool pour en trouver une
// liste chainée ? freeList ? un simple push pop et partir du principe que les particules in use sont à la fin ?

// Exemple d'utilisation

// 1) Déclaration
// ParticlePool _emiter;

// 2) Appel (sur un press, repeat, une collision)
// pour creer une Particule centrée en X dans le 1er tiers de l'ecran 
// avec une velocitéX qui varie entre -5 & 5
// avec une vélocitéY qui varie entre -5 & -10
// et 100 frame de durée de vie avant de retourner dans la pool
// _emiter.create(STAGEWIDTH2, STAGEHEIGHT * 1 / 3, random(-5, 5), random(-5,-10), 100);

// 3) update des valeurs, pour temps, on peux cumuler les durées de frame genre temps += DuréeDuneFrame
// _emiter.update(temps);


class Particle
{
public:
    Particle()
        : _framesLeft(0)
    {
    }

    void init(int16_t pX, int16_t pY,
              float pXvel, float pYvel, uint8_t pLifetime, Color pColor)
    {
        _x = pX;
        _y = pY;
        _xVel = pXvel;
        _yVel = pYvel;
        _framesLeft = pLifetime;
        _color = pColor;
    }

    void update(float pTps)
    {
        if (!inUse())
            return;

        _framesLeft--;
        
        // exemple de parametres
        //_yVel += 1;   // gravite
        _xVel *= .95; // friction
        _yVel *= .95;
        _x += _xVel;
        _y += _yVel;

        // la particule peut se dessiner elle même dans son update, ou alors on laisse la pool gerer l'affichage aussi
        gb.display.setColor(_color);
        gb.display.fillCircle(_x, _y, min(_framesLeft, 3));
    }

    bool inUse() const { return _framesLeft > 0; }

private:
    int16_t _x, _y;
    float _xVel, _yVel;
    uint8_t _framesLeft;
    Color _color;
};

class ParticlePool
{
public:
    void create(int16_t pX, int16_t pY,
                float pXvel, float pYvel, uint8_t pLifetime, Color pColor = WHITE)
    {
        for (uint8_t i = 0; i < POOL_SIZE; i++)
        {
            if (!_particles[i].inUse())
            {
                _particles[i].init(pX, pY, pXvel, pYvel, pLifetime, pColor);
                return;
            }
        }
    }

    void burst(int16_t pX, int16_t pY,
                float pXvel, float pYvel, uint8_t pLifetime, Color pColor = WHITE)
    {
        uint8_t burstLimit = 0;
        for (uint8_t i = 0; i < POOL_SIZE; i++)
        {
            if (!_particles[i].inUse())
            {
                randomSeed(random(-1024, 1024));
                _particles[i].init(pX, pY, random(-4, 4), random(-4, 4), random(max(5, pLifetime), pLifetime), pColor);
                if(burstLimit++ > POOL_SIZE2){
                    return;
                }
            }
        }
    }

    void update(float pTps)
    {
        for (uint8_t i = 0; i < POOL_SIZE; i++)
        {
            _particles[i].update(pTps);
        }
    }

private:
    static const uint8_t POOL_SIZE = 12;
    static const uint8_t POOL_SIZE2 = 6;
    Particle _particles[POOL_SIZE];
};

#endif