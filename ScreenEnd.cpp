#include <Gamebuino-Meta.h>
#include "BlankTemplate.h"
#include "ScreenEnd.h"
#include "Config.h"

void ScreenEnd::init()
{
    _screenName = "ScreenEnd";
    _nextScreen = "ScreenGame";
}

void ScreenEnd::start()
{
    BaseScreen::start();
    transitionIN();
}

void ScreenEnd::onEnterFrame()
{
    switch (_screenState)
    {
    case SCREEN_MAIN:
        gb.display.clear();
        updateMainState();
        break;

    case SCREEN_TRANSITION_IN:
        updateTransitionInState();
        break;

    case SCREEN_TRANSITION_OUT:
        updateTransitionOutState();
        break;
    }

    if (gb.buttons.repeat(BUTTON_A, 0))
    {
        gb.display.setCursor(0, 50);
        gb.display.print("Name: ");
        gb.display.print(getScreenName());

        gb.display.setCursor(0, 60);
        gb.display.print("screenState: ");
        gb.display.print(_screenState);
    }

    if (gb.buttons.pressed(BUTTON_MENU))
    {
        transitionOUT();
    }
}

void ScreenEnd::end()
{
    gb.display.clear();
    gb.display.print("end !");
    _blankTemplate.loadScreen(_nextScreen);
}

////////////////////////////////////////////////
// TRANSITION IN
void ScreenEnd::transitionIN()
{
    gb.display.clear();
    this->_transitionFrameCountLimit = 44; // 220 / 5
    BaseScreen::transitionIN();            // change le state du screen
}

void ScreenEnd::updateTransitionInState()
{
    if (_transitionFrameCountLimit > 0)
    {
        gb.display.print(">E>E>");
        _transitionFrameCountLimit--;
    }
    else
    {
        _screenState = SCREEN_MAIN;
    }
}

////////////////////////////////////////////////
// MAIN STATE
void ScreenEnd::updateMainState()
{
    gb.display.print(getScreenName());
    gb.display.print(" ");
    gb.display.print("Main State");
}

////////////////////////////////////////////////
// TRANSITION OUT
void ScreenEnd::transitionOUT()
{
    gb.display.clear();
    this->_transitionFrameCountLimit = 44; // 220 / 5
    BaseScreen::transitionOUT();           // change le state du screen
}

void ScreenEnd::updateTransitionOutState()
{
    if (_transitionFrameCountLimit > 0)
    {
        gb.display.print("<END<");
        _transitionFrameCountLimit--;
    }
    else
    {
        end();
    }
}