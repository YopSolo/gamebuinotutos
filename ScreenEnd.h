#ifndef BLANKTEMPLATE_SCREENEND_H_
#define BLANKTEMPLATE_SCREENEND_H_
#include "BaseScreen.h"

class ScreenEnd : virtual public BaseScreen
{
public:
    ScreenEnd(BlankTemplate &pBlankTemplate)
        : _blankTemplate(pBlankTemplate)
    {}

    void init();
    void start();
    void onEnterFrame();
    void end();

    void transitionIN();
    void transitionOUT();

    void updateMainState();
    void updateTransitionInState();
    void updateTransitionOutState();

private:
    BlankTemplate &_blankTemplate;
};

#endif