#include <Gamebuino-Meta.h>
#include "BlankTemplate.h"
#include "ScreenGame.h"
#include "Config.h"

void ScreenGame::init()
{
    _screenName = "ScreenGame";
    _nextScreen = "ScreenEnd";
}

void ScreenGame::start()
{
    BaseScreen::start();
    transitionIN();
}

void ScreenGame::onEnterFrame()
{
    switch (_screenState)
    {
    case SCREEN_MAIN:
        gb.display.clear();
        updateMainState();
        break;

    case SCREEN_TRANSITION_IN:
        updateTransitionInState();
        break;

    case SCREEN_TRANSITION_OUT:
        updateTransitionOutState();
        break;
    }

    if (gb.buttons.repeat(BUTTON_A, 0))
    {
        gb.display.setCursor(0, 50);
        gb.display.print("Name: ");
        gb.display.print(getScreenName());

        gb.display.setCursor(0, 60);
        gb.display.print("screenState: ");
        gb.display.print(_screenState);
    }

    if (gb.buttons.pressed(BUTTON_MENU))
    {
        transitionOUT();
    }
}

void ScreenGame::end()
{
    gb.display.clear();
    gb.display.print("end !");
    _blankTemplate.loadScreen(_nextScreen);
}

////////////////////////////////////////////////
// TRANSITION IN
void ScreenGame::transitionIN()
{
    gb.display.clear();
    this->_transitionFrameCountLimit = 44; // 220 / 5
    BaseScreen::transitionIN();            // change le state du screen
}

void ScreenGame::updateTransitionInState()
{
    if (_transitionFrameCountLimit > 0)
    {
        gb.display.print(">M>M>");
        _transitionFrameCountLimit--;
    }
    else
    {
        _screenState = SCREEN_MAIN;
    }
}

////////////////////////////////////////////////
// MAIN STATE
void ScreenGame::updateMainState()
{
    gb.display.print(getScreenName());
    gb.display.print(" ");
    gb.display.print("Main State");
}

////////////////////////////////////////////////
// TRANSITION OUT
void ScreenGame::transitionOUT()
{
    gb.display.clear();
    this->_transitionFrameCountLimit = 44; // 220 / 5
    BaseScreen::transitionOUT();           // change le state du screen
}

void ScreenGame::updateTransitionOutState()
{
    if (_transitionFrameCountLimit > 0)
    {
        gb.display.print("<MAIN");
        _transitionFrameCountLimit--;
    }
    else
    {
        end();
    }
}