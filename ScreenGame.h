#ifndef BLANKTEMPLATE_SCREENGAME_H_
#define BLANKTEMPLATE_SCREENGAME_H_
#include "BaseScreen.h"

class ScreenGame : virtual public BaseScreen
{
public:
    ScreenGame(BlankTemplate &pBlankTemplate)
        : _blankTemplate(pBlankTemplate)
    {}

    void init();
    void start();
    void onEnterFrame();
    void end();

    void transitionIN();
    void transitionOUT();

    void updateMainState();
    void updateTransitionInState();
    void updateTransitionOutState();

private:
    BlankTemplate &_blankTemplate;
};

#endif