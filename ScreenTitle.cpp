#include <Gamebuino-Meta.h>
#include "BlankTemplate.h"
#include "MMath.h"
#include "BT.h"
#include "ScreenTitle.h"
#include "Config.h"

void ScreenTitle::init()
{
    _screenName = "ScreenTitle";
    _nextScreen = "ScreenTitle";
}

void ScreenTitle::start()
{
    BaseScreen::start();
    _testX = 0;
    _test2X = 0;
    transitionIN();
}

void ScreenTitle::onEnterFrame()
{
    switch (_screenState)
    {
    case SCREEN_MAIN:
        if (!_isFlashing)
        {
            gb.display.clear();
        }else{
            gb.display.clear(_flashColor);
        }
        updateMainState();
        break;

    case SCREEN_TRANSITION_IN:
        updateTransitionInState();
        break;

    case SCREEN_TRANSITION_OUT:
        updateTransitionOutState();
        break;
    }

    if (gb.buttons.pressed(BUTTON_A))
    {
        flash(DARKGRAY);
    }

    if (gb.buttons.pressed(BUTTON_B))
    {
        shake(10, 5);
        lights(YELLOW);
        _emiter3.burst(
            STAGEWIDTH2,
            STAGEHEIGHT2,
            0, // xVel
            0, // // yVel
            8, // ttl en frames
            YELLOW);
    }

    if (gb.buttons.pressed(BUTTON_MENU))
    {
        transitionOUT();
    }
}

void ScreenTitle::end()
{
    gb.display.clear();
    gb.display.print(getScreenName());
    gb.display.print(" end !");
    gb.display.print(" loadScreen ");
    gb.display.print(_nextScreen);

    _blankTemplate.loadScreen(_nextScreen);
}

////////////////////////////////////////////////
// TRANSITION IN
void ScreenTitle::transitionIN()
{
    gb.display.clear();
    this->_transitionFrameCountLimit = 21;
    BaseScreen::transitionIN(); // change le state du screen
}

void ScreenTitle::updateTransitionInState()
{
    gb.display.setFontSize(1);
    if (_transitionFrameCountLimit > 0)
    {
        String maze = "";
        for (uint8_t i = 0; i < 40; i++)
        {
            if (random(0, 10) > 5)
            {
                maze.concat('/');
            }
            else
            {
                maze.concat("\\");
            }
        }
        gb.display.print(maze);

        _transitionFrameCountLimit--;
    }
    else
    {
        _screenState = SCREEN_MAIN;
    }
}

////////////////////////////////////////////////
// MAIN STATE
void ScreenTitle::updateMainState()
{
    gb.display.setCursor(_shakeX, _shakeY);
    gb.display.setColor(WHITE);
    // gb.display.print(getScreenName());
    // gb.display.print(" ");
    // gb.display.print("Main State");
    // gb.display.print(" ");
    String tw = getScreenName();
    tw.concat(" ");
    tw.concat("Main State");
    tw.concat(" ");
    typewrite(tw, 2, 2);

    for (uint16_t c = 0; c < 16; c++)
    {
        gb.display.setColor(gb.display.getPalette()[c]);
        gb.display.fillRect(1 + (c * 8), STAGEHEIGHT - 8, 8, 8);
    }
    gb.display.setColor(WHITE);
    gb.display.drawRect(0, STAGEHEIGHT - 9, (16 * 8) + 2 , 10);

    float step = 160 / 80;
    if (_testX + step < STAGEWIDTH)
    {
        _testX += step;
    }

    //gb.display.setColor(WHITE);
    //gb.display.drawCircle(MMath::lerp(0, STAGEWIDTH, _tesstX / STAGEWIDTH), 20, 10);

    gb.display.setColor(YELLOW);
    gb.display.drawCircle(MMath::easeIN(0, STAGEWIDTH, _testX) * STAGEWIDTH, 50, 10);

    gb.display.setColor(GREEN);
    gb.display.drawCircle(MMath::easeOUT(0, STAGEWIDTH, _testX) * STAGEWIDTH, 80, 10);

    //gb.display.setColor(RED);
    //_test2X += ((STAGEWIDTH - _test2X)) * .1;
    //gb.display.drawCircle(_test2X, 110, 10);

    _emiter1.create(
        -10 + MMath::easeIN(0, STAGEWIDTH, _testX) * STAGEWIDTH,
        50,
        -1,                              // xVel
        MMath::ran(-3, 3, gb.frameCount), // yVel
        15,                              // ttl en frames
        gb.display.getPalette()[gb.frameCount % 8]);
    _emiter1.update(dt);

    _emiter2.create(
        -10 + MMath::easeOUT(0, STAGEWIDTH, _testX) * STAGEWIDTH,
        80,
        -1,                              // xVel
        MMath::ran(-3, 3, gb.frameCount), // yVel
        15,                              // ttl en frames
        gb.display.getPalette()[8 + gb.frameCount % 8]);
    _emiter2.update(dt);

    //
    _emiter3.update(dt);

    BaseScreen::updateMainState(); // pour gerer les features du baseScreen
}

////////////////////////////////////////////////
// TRANSITION OUT
void ScreenTitle::transitionOUT()
{
    gb.display.clear();
    this->_transitionFrameCountLimit = 21;
    BaseScreen::transitionOUT(); // change le state du screen
}

void ScreenTitle::updateTransitionOutState()
{
    gb.display.clear();
    gb.display.setFontSize(1);

    if (_transitionFrameCountLimit > 0)
    {
        String maze = "";
        for (uint16_t i = 0; i < 40 * _transitionFrameCountLimit; i++)
        {
            // if( random(0,10)>5 ){
            //     maze.concat('/');
            // }else{
            //     maze.concat("\\");
            // }
            maze.concat('0');
        }
        gb.display.print(maze);
        _transitionFrameCountLimit--;
    }
    else
    {
        end();
    }
}