#ifndef BLANKTEMPLATE_SCREENTITLE_H_
#define BLANKTEMPLATE_SCREENTITLE_H_
#include "BaseScreen.h"
#include "ParticlePool.h"

class ScreenTitle : public BaseScreen
{
public:
    ScreenTitle(BlankTemplate &pBlankTemplate)
        : _blankTemplate(pBlankTemplate)
    {}

    void init();
    void start();
    void onEnterFrame();
    void end();

    void transitionIN();
    void transitionOUT();

    void updateMainState();
    void updateTransitionInState();
    void updateTransitionOutState();

private:
    BlankTemplate &_blankTemplate;
    ParticlePool _emiter1;
    ParticlePool _emiter2;
    ParticlePool _emiter3;

    float _testX = 0;
    float _test2X = 0;
};
#endif