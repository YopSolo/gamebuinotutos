#pragma once


// #define SAVECONF_DEFAULT_BLOBSIZE (66U)

// General Settings
// #define FOLDER_NAME // (string) - name of the folder in which the binary files will be located on the sd card, defaults to sketch name. Must be at least 4 chars long

// Save Settings
// #define SAVEBLOCK_NUM // (int) - number of saveblocks (for a more detailed explenation, see my gb.save explenation), defaults to 64
// #define SAVEFILE_NAME // (string)- name of the savefile, defaults to "save.sav"
// #define SAVECONF_DEFAULT_BLOBSIZE // (int) - size of a save blob, if not specified differently via saveconf  (for a more detailed explenation, see my gb.save explenation), defaults to 32

// Display Settings
// #define DISPLAY_MODE DISPLAY_MODE_RGB565             // 80x64        216 couleurs
#define DISPLAY_MODE DISPLAY_MODE_INDEX                 // 160x128      16 couleurs indexées
// #define DISPLAY_MODE DISPLAY_MODE_INDEX_HALFRES       // 80x64        16 couleurs indexées

// ne marche pas :/
// #define DISPLAY_DEFAULT_BACKGROUND_COLOR Color::darkblue
// #define DISPLAY_DEFAULT_COLOR Color::orange

// #define DISPLAY_CONSTRUCTOR //- An Image constructor to use for gb.display, default depends on DISPLAY_MODE

// Record Settings
// #define MAX_IMAGE_RECORDING // (int) - Maximum number of images that can be recorded at the same time, defaults to 2

// Sound Settings
// #define SOUND_CHANNELS // (int) - number of sound sources that can be played at the same time, defaults to 4
// #define SOUND_FREQ // (int) - frequency at which the interrupt runs, default is 44100