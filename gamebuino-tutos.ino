#include "Config.h"
#include "BlankTemplate.h"

BlankTemplate *blankTemplate;

void setup()
{
    gb.begin();
    gb.setFrameRate(FRAME_RATE);

    blankTemplate = new BlankTemplate();
    blankTemplate->begin();
}

void loop()
{
    while (!gb.update());    
    blankTemplate->update();
}